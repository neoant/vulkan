// Vulkan graphics and compute API
// Copyright (C) 2021  John Doe <iocdr@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![cfg_attr(not(feature = "debug"), forbid(clippy::all))]
#![cfg_attr(not(feature = "debug"), forbid(clippy::nursery))]
#![cfg_attr(not(feature = "debug"), forbid(clippy::pedantic))]
#![cfg_attr(not(feature = "debug"), forbid(clippy::cargo))]
#![cfg_attr(not(feature = "debug"), forbid(nonstandard_style))]
#![cfg_attr(not(feature = "debug"), deny(warnings))]
#![cfg_attr(not(feature = "debug"), deny(rust_2018_idioms))]
#![cfg_attr(not(feature = "debug"), deny(unused))]
#![cfg_attr(not(feature = "debug"), forbid(future_incompatible))]
#![cfg_attr(not(feature = "debug"), forbid(box_pointers))]
#![cfg_attr(not(feature = "debug"), forbid(elided_lifetimes_in_paths))]
#![cfg_attr(not(feature = "debug"), forbid(missing_copy_implementations))]
#![cfg_attr(not(feature = "debug"), forbid(missing_debug_implementations))]
#![cfg_attr(not(feature = "debug"), forbid(missing_docs))]
#![cfg_attr(not(feature = "debug"), allow(single_use_lifetimes))]
#![cfg_attr(not(feature = "debug"), forbid(trivial_casts))]
#![cfg_attr(not(feature = "debug"), forbid(trivial_numeric_casts))]
#![cfg_attr(not(feature = "debug"), forbid(unused_import_braces))]
#![cfg_attr(not(feature = "debug"), deny(unused_qualifications))]
#![cfg_attr(not(feature = "debug"), forbid(unused_results))]
#![cfg_attr(not(feature = "debug"), forbid(variant_size_differences))]

//! Vulkan

mod codegen;
mod parse;
mod types;

use codegen::generate;
use log::trace;
use parse::parse;
use roxmltree::Document;
use std::env::args;
use std::fs::read_to_string;

/// Entry point for the program.
pub fn main() {
    trace!("BEGIN vulkan");

    if args().nth(1).is_none() {
        eprintln!("\x1b[31mERROR:\x1b[0m Need filepath of vk.xml as argument to the program");
        eprintln!("");
        eprintln!("\x1b[1mUSAGE:\x1b[0m vulkan /path/to/vk.xml");
        panic!("invalid use of program");
    }

    let path = args().nth(1).unwrap();

    let contents = read_to_string(path.clone());

    if contents.is_err() {
        eprintln!("\x1b[31mERROR:\x1b[0m Unable to read file: {}", path);
        eprintln!("");
        eprintln!("\x1b[1mUSAGE:\x1b[0m vulkan /path/to/vk.xml");
        panic!("{:?}", contents.err());
    }

    let contents = contents.unwrap();
    let document = Document::parse(&contents).expect("failed to parse vk.xml file");
    let ast = parse(document.root_element());

    // dbg!(&ast);

    let output = generate(ast);

    trace!("END wayland-scanner");

    println!("{}", output);
}
